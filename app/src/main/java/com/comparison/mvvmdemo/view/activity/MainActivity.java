package com.comparison.mvvmdemo.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.comparison.mvvmdemo.R;
import com.comparison.mvvmdemo.view.adapter.TVShowsAdapter;
import com.comparison.mvvmdemo.databinding.ActivityMainBinding;
import com.comparison.mvvmdemo.model.TVShow;
import com.comparison.mvvmdemo.viewmodels.TVShowsViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding activityMainBinding;
    private TVShowsViewModel viewModel;
    private List<TVShow> tvShows = new ArrayList<>();
    private TVShowsAdapter tvShowsAdapter;
    private int currentPage = 1;
    private int totalPages = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        activityMainBinding.imageSearch.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
        });

        init();
    }

    private void init() {
        //recyclerView
        activityMainBinding.tvShowRV.setHasFixedSize(true);
        viewModel = new ViewModelProvider(this).get(TVShowsViewModel.class);
        tvShowsAdapter = new TVShowsAdapter(tvShows);
        activityMainBinding.tvShowRV.setAdapter(tvShowsAdapter);

        //pagination
        activityMainBinding.tvShowRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (activityMainBinding.tvShowRV.canScrollVertically(1)) {
                    if (currentPage <= totalPages) {
                        currentPage += 1;
                        getMostPopularTVShows();
                    }
                }
            }
        });

        getMostPopularTVShows();
    }


    private void getMostPopularTVShows() {
        //loading
        isLoading();
        viewModel.getMostPopularTVShows(currentPage).observe(this, mostPopularTVShowResponse -> {
            isLoading();
            if (mostPopularTVShowResponse != null) {
                totalPages = mostPopularTVShowResponse.getTotalPages();
                if (mostPopularTVShowResponse.getTvShows() != null) {
                    int oldCount = tvShows.size();
                    tvShows.addAll(mostPopularTVShowResponse.getTvShows());
                    tvShowsAdapter.notifyItemRangeInserted(oldCount, tvShows.size());
                }
            }
        });
    }

    private void isLoading() {
        if (currentPage == 1) {
            if (activityMainBinding.getIsLoading() != null && activityMainBinding.getIsLoading()) {
                activityMainBinding.setIsLoading(false);
            } else {
                activityMainBinding.setIsLoading(true);
            }
        } else {
            if (activityMainBinding.getIsLoadingMore() != null && activityMainBinding.getIsLoadingMore()) {
                activityMainBinding.setIsLoadingMore(false);
            } else {
                activityMainBinding.setIsLoadingMore(true);
            }
        }
    }
}