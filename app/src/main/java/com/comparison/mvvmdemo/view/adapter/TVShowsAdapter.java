package com.comparison.mvvmdemo.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.comparison.mvvmdemo.R;
import com.comparison.mvvmdemo.databinding.ItemTvShowsListBinding;
import com.comparison.mvvmdemo.model.TVShow;

import java.util.List;

public class TVShowsAdapter extends RecyclerView.Adapter<TVShowsAdapter.TVShowViewHolder> {
    private List<TVShow> tvShows;

    public TVShowsAdapter(List<TVShow> tvShows) {
        this.tvShows = tvShows;
    }

    @NonNull
    @Override
    public TVShowsAdapter.TVShowViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {

        ItemTvShowsListBinding tvShowBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_tv_shows_list, parent, false
        );
        return new TVShowViewHolder(tvShowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TVShowsAdapter.TVShowViewHolder holder, int position) {
        holder.bindTVShow(tvShows.get(position));

    }

    @Override
    public int getItemCount() {
        return tvShows.size();
    }

    class TVShowViewHolder extends RecyclerView.ViewHolder{
        private ItemTvShowsListBinding itemTvShowsListBinding;

        public TVShowViewHolder(ItemTvShowsListBinding itemTvShowsListBinding) {
            super(itemTvShowsListBinding.getRoot());
            this.itemTvShowsListBinding = itemTvShowsListBinding;
        }

        public void bindTVShow(TVShow tvShow){
            itemTvShowsListBinding.setTvShow(tvShow);
            itemTvShowsListBinding.executePendingBindings();
            //itemContainerTvShowBinding.getRoot().setOnClickListener(v -> tvShowsListener.onTVShowClicked(tvShow));
        }
    }
}
