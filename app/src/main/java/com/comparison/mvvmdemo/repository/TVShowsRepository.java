package com.comparison.mvvmdemo.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.comparison.mvvmdemo.network.ApiClient;
import com.comparison.mvvmdemo.network.ApiService;
import com.comparison.mvvmdemo.responses.TVShowResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TVShowsRepository {
    private ApiService apiService;

    //constructor
    public TVShowsRepository() {
        apiService = ApiClient.getRetrofit().create(ApiService.class);
    }

    //live data return type method
    public LiveData<TVShowResponse> getTvShows(int page) {
        MutableLiveData<TVShowResponse> data = new MutableLiveData<>();
        apiService.getMostPopularTVShows(page).enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(@NonNull Call<TVShowResponse> call, @NonNull Response<TVShowResponse> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<TVShowResponse> call, @NonNull Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }

}
