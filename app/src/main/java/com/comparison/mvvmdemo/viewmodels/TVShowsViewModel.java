package com.comparison.mvvmdemo.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.comparison.mvvmdemo.repository.TVShowsRepository;
import com.comparison.mvvmdemo.responses.TVShowResponse;

public class TVShowsViewModel extends ViewModel {

    private TVShowsRepository tvShowsRepository;

    //constructor of view model
    public TVShowsViewModel() {
        tvShowsRepository = new TVShowsRepository();
    }

    //live data return type method
    public LiveData<TVShowResponse> getMostPopularTVShows(int page) {
        return tvShowsRepository.getTvShows(page);
    }
}
