package com.comparison.mvvmdemo.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.comparison.mvvmdemo.repository.SearchTVShowRepository;
import com.comparison.mvvmdemo.responses.TVShowResponse;

public class SearchViewModel extends ViewModel {
    private SearchTVShowRepository searchTVShowRepository;

    public SearchViewModel() {
        searchTVShowRepository = new SearchTVShowRepository();
    }

    public LiveData<TVShowResponse> getSearchItem(String query, int page) {
        return searchTVShowRepository.getSearchItem(query, page);
    }
}
